<?php
class conexion{
	function __construct(){
		try{
			//Declaracion de variables.
			$host="localhost";
			$db_name="pruebabicico";
			$usuario="root";
			$pass="";

			//Cadena de conexion.
		$this->con=mysqli_connect($host, $usuario, $pass) or die ("Error en la conexion a la base de datos");
		//Seleccion de la base de datos.
		mysqli_select_db($this->con,$db_name) or die ("No se ah encontrado la base de datos.");

		//echo "Conexion exitosa";

		}catch(Exception $ex){
			throw $ex;
		}
  		
		}
		//Se crea la funcion de consultar a la base de datos.
		function consultar($sql){
			
			$resultado=mysqli_query($this->con, $sql);
			$data=NULL;
			while($fila=mysqli_fetch_assoc($resultado)){

				$data[]=$fila;
				 
			}
			return $data;
		}
        function actualizar($sql){
        	
        	mysqli_query($this->con,$sql);
        	if(mysqli_affected_rows($this->con)<=0){
        		//echo "No se pudo realizar lo pedido.";
        	}else{
        		
        		//echo "Se han realizado los cambios corectamente.";
        	}
        }

  }
?>