<?php
require "conexion.php";
$obj = new conexion();
$datos=$obj->consultar("SELECT * FROM stations");
?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta charset="utf-8">

</head>
<center><h2>Estaciones de bicicletas</h2></center>
<body>
<table class="table table-responsive table-bordered">
    <thead>
    <tr>
        <th>Clave</th>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Latitud</th>
        <th>Longitud</th>
        <th>Bicicletas Habilitadas</th>
        <th>Capacidad Maxima</th>
    </tr>
    </thead>
    <?php foreach($datos as $fila){ ?>
    <tbody>
    <tr>
        <td><?php echo $fila["id"] ?></td>
        <td><?php echo $fila["name"] ?></td>
        <td><?php echo $fila["address"] ?></td>
        <td><?php echo $fila["lat"] ?></td>
        <td><?php echo $fila["lng"] ?></td>
        <td><?php echo $fila["availables_bikes"] ?></td>
        <td><?php echo $fila["max_storage_capacity"] ?></td>
    </tr>
    </tbody>
    <?php } ?>
</table>
</body>
</html>